

@extends('main')

@section('content')

    @if(Auth::check())
    <div class="row">

        <div class="col-md-8 col-md-offset-2">
            <h1>Create a new Reservation</h1>
            <hr>

            {!!Form::open(['route' => 'reservations.store']) !!}

            {{ Form::label('name', "Name:") }}
            {{ Form::text('name',Auth::user()->getFullName(), array('class' => 'form-control')) }}

            {{ Form::label('phone_number', "Phone number:") }}
            {{ Form::text('phone_number',Auth::user()->telephone, array('class' => 'form-control')) }}

            {{ Form::label('date', "Date:") }}
            {{ Form::date('date',null, array('class' => 'form-control')) }}

            {{ Form::label('time', "Time:") }}
            {{ Form::time('time', null, array('class' => 'form-control')) }}

            {{ Form::label('number_of_people', "Number of people:") }}
            {{ Form::number('number of people',null , array('class' => 'form-control')) }}


            {{Form::submit('create reservation', array('class'=> 'btn btn-success btn-lg btn-block', 'style' => 'margin-top:20px'))}}
            {!! Form::close() !!}
        @else

                <div class="row">

                 <div class="col-md-8"></div>
                    <h1>You need to be logged in to make a reservation</h1>
                    <button>{!! Html::linkRoute('login', 'Login here', array('class'=>"btn btn-danger btn-block")) !!}</button>


                </div>

        @endif
        </div>


    </div>




@endsection