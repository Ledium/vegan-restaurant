@extends('main')


@section('content')

    @if (Auth::check() && Auth::user()->isAdmin())
    <div class="row">
        <div class="col-md-10">
            <h1>All reservations</h1>
        </div>


        <hr>
    </div>

    <div class="class=row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <th>#</th>
                    <th>name</th>
                    <th>Phone number:</th>
                    <th>Date:</th>
                    <th>Time:</th>
                    <th>Number of people</th>
                    <th></th>
                </thead>
                <tbody>
                @foreach($reservations as $reservation)

                    <tr>
                        <th>{{$reservation->id}}</th>
                        <td>{{$reservation->name}}</td>
                        <td>{{$reservation->phone_number}}</td>
                        <td>{{$reservation->date}}</td>
                        <td>{{$reservation->time}}</td>
                        <td>{{$reservation->number_of_people}}</td>
                        <td>{{substr($reservation->body, 0, 50)}} {{ strlen($reservation->body) > 50 ? "...": " " }}</td>
                        <td>{{date( 'M j, Y', strtotime($reservation->created_at)) }}</td>
                        <td><a href="{{route('reservations.show', $reservation->id)}}" class="btn btn-default btn-sm">view</a> <a href="{{ route('reservations.edit', $reservation->id) }}"class="btn btn-default btn-sm">edit</a></td>
                    </tr>


                 @endforeach

                </tbody>
            </table>
        </div>
    </div>


    @endif


@endsection