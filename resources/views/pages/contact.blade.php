@extends ('main')

@section('content')
    <div class="row">
        <div class=" col-md-offset-3 col-md-6">
            <h1>Contact Us</h1>
            <hr>

            
                    <div class="info">
                        <p>Our restorant is a one of a kind vegan establishment that will likely cater to anyones needs and food related desires. Find a vast variety of everyday ingredients lifted to new level for only your enjoyment.</p>
                    </div>
                   
            <form>
                <div class="form-group">
                    <label name="email">Email:</label>
                    <input id="email" name="email" class="form-control">
                </div>

                <div class="form-group">
                    <label name="subject">Subject:</label>
                    <input id="subject" name="subject" class="form-control">
                </div>

                <div class="form-group">
                    <label name="message">Message:</label>
                    <textarea id="message" name="message" class="form-control">Type your message here...</textarea>
                </div>

                <input type="submit" value="Send Message" class="btn btn-success">
            </form>
        </div>
    </div>

    <div class="row">
        <div class=" col-md-offset-3 col-md-6">
        <style >
            #map {
                height:180px;
                width:100%;
            }
        </style>


            <div id="map"></div>
            <script>
                function initMap() {
                    var uluru = {lat: 54.679272, lng: 25.267189};
                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 16,
                        center: uluru
                    });
                    var marker = new google.maps.Marker({
                        position: uluru,
                        map: map
                    });
                }
            </script>
            <script async defer
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXDNqPLpKg6WiSIyKp77RGLmlvvBK017k&callback=initMap">
            </script>
        </div>
    </div>
@endsection