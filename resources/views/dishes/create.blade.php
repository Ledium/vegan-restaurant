@extends('main')

@section('content')


    @if (Auth::check() && Auth::user()->isAdmin())
    <div class="row">

    <div class="col-md-8 col-md-offset-2">
        <h1>Create new Dish</h1>
        <hr>

        {!! Form::open(['route' => 'dishes.store', 'files' => 1]) !!}
        {{ Form::label('title', "Title:") }}
        {{ Form::text('title', null, array('class' => 'form-control')) }}
        {{ Form::label('description', "Dish description")}}
        {{ Form::textarea('description', null, array('class' => 'form-control') ) }}
        {{ Form::label('image', 'Upload an image')}}
        {{ Form::file('image', null, array('class' => 'form-control'))}}
        {{Form::label('price', "Price:")}}
        {{Form::text('price', null)}}
        {{ Form::submit('çreate dish', array('class'=> 'btn btn-success btn-lg btn-block', 'style' => 'margin-top:20px'))}}
        {!! Form::close() !!}



    </div>


    </div>
    @else
        <div>

            <div class="row">

                <div class="col-md-8"></div>
                <h1>You need to be logged in to access this page</h1>
                <button class="btn btn-success">{!! Html::linkRoute('login', 'Login here') !!}</button>


            </div>

         </div>
    @endif
    @endsection