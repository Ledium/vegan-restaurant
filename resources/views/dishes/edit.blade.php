@extends('main')

@section('content')
    @if (Auth::check() && Auth::user()->isAdmin())
    <div class="row">

        {!! Form::model($dish, ['route' => ['dishes.update', $dish->id], 'method' => 'PATCH', 'files' => 1]) !!}
        <div class="col-md-8">
            {{Form::label('title', 'Title:' ) }}
            {{ Form::text('title', null, ['class' => 'form-control input-lg'] ) }}
            {{Form::label('description', 'Description:') }}
            {{Form::textarea('description', null, ['class' => 'form-control input-lg'])}}
            {{Form::label('image', 'Image:') }}
            @if ($dish->image)
                <div>
                    <img src="{{Storage::url($dish->image)}}" class="img-responsive">
                </div>
            @endif
            {{Form::file('image', null, array('class' => 'form-control'))}}




        </div>
        <div class="col-md-4">
            <div class="well">
                <dl class="dl-horizontal">
                    <dt>Created at: </dt>
                    <dd> {{ date('M j, Y, H:i', strtotime($dish->created_at)) }}</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Last updated: </dt>
                    <dd> {{ date('M j, Y, H:i', strtotime($dish->updated_at)) }}</dd>
                </dl>
                <hr>
                <div class="row">
                    <div class="col-sm-6">
                        {!! Html::linkRoute('dishes.show', 'Cancel', array($dish->id), array('class'=>"btn btn-danger btn-block")) !!}

                    </div>
                    <div class="col-sm-6">
                        {{ Form::submit('Save changes', ['class' => 'btn btn-success btn-block' ]) }}


                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>


@endif




@endsection