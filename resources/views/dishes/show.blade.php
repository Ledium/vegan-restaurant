@extends('main')

@if (Auth::check() && Auth::user()->isAdmin())

@section('content')
    <div class="row">
        <div class="col-md-8">
            <h1>  {{$dish->title }}</h1>

            <p class="lead">{{$dish->description}}</p>
        </div>
        <div class="col-md-4">
            <div class="well">
                <dl class="dl-horizontal">
                    <dt>Created at: </dt>
                    <dd> {{ date('M j, Y, H:i', strtotime($dish->created_at)) }}</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>Last updated: </dt>
                    <dd> {{ date('M j, Y, H:i', strtotime($dish->updated_at)) }}</dd>
                </dl>
                <hr>
                <div class="row">
                    <div class="col-sm-6">
                        {!! Html::linkRoute('dishes.edit', 'Edit', array($dish->id), array('class'=>"btn btn-primary btn-block")) !!}

                    </div>
                    <div class="col-sm-6">
                        {!! Html::linkRoute('dishes.destroy', 'Delete', array($dish->id), array('class'=>"btn btn-danger btn-block")) !!}

                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
    @endif
@endsection