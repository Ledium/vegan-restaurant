@extends ('main')

@section('content')


@if (Auth::check() && Auth::user()->isAdmin())


    <div class="row">
        <div class="col-md-10">
            <h1>All dishes</h1>
        </div>

        <div class="col-md-2">
            <a href="{{route('dishes.create')}}" class="btn btn-xl btn-block btn-primary btn-h1-spacing">Create New Dish</a>
        </div>
        <hr>
    </div>

    <div class="class=row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                <th>#</th>
                <th>Title</th>
                <th>Description</th>
                <th>Created at:</th>
                <th></th>
                </thead>
                <tbody>
                @foreach($dishes as $dish)

                    <tr>
                        <th>{{$dish->id}}</th>
                        <td>{{$dish->title}}</td>
                        <td>{{substr($dish->description, 0, 50)}} {{ strlen($dish->body) > 50 ? "...": " " }}</td>
                        <td>{{date( 'M j, Y', strtotime($dish->created_at)) }}</td>
                        <td>{{$dish->image}}</td>
                        <td><a href="{{route('dishes.show', $dish->id)}}" class="btn btn-default btn-sm">view</a> <a href="{{ route('dishes.edit', $dish->id) }}"class="btn btn-default btn-sm">edit</a></td>
                        <td><a href="{{ route('dishes.destroy', $dish->id) }}"class="btn btn-danger btn-sm">delete</a></td>
                    </tr>


                @endforeach

                </tbody>
            </table>
        </div>
    </div>


@else


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="dish_menu">
                @foreach ($dishes as $dish)

                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail" >
                            <h3>{{$dish->title}}</h3>
                            <img src="{{Storage::url($dish->image)}}" class="img-responsive">
                            <div class="caption">
                                <div class="row">
                                    <div class="col-md-6 col-xs-6">

                                    </div>
                                </div>
                                <p>{{$dish->description}}</p>
                                <h4>{{$dish->price}} &euro;</h4>
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3">

                                        {!! Form::model($dish, ['route' => ['cart.add', $dish->id], 'method' => 'POST']) !!}

                                        {{ Form::submit('Buy', array('class'=> 'btn btn-success btn-block fa fa-shopping-cart', 'style' => 'margin-top:20px'))}}
                                        {!! Form::close() !!}

                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        </div>
    </div>
    @endif


@endsection