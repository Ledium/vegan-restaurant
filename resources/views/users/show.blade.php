@extends('main')



@section('content')

    @if (Auth::check() && Auth::user()->isAdmin())
    <div class="row">
    <div class="col-md-8">>
    <h1>  {{ $profile->title }}</h1>

    <p class="lead">{{$profile->name}}</p>
        <p class="lead">{{$profile->phone_number}}</p>
        <p class="lead">{{$profile->number_of_people}}</p>
        <p class="lead">{{$profile->time}}</p>
        <p class="lead">{{$profile->date}}</p>
     </div>
        <div class="col-md-4">
            <div class="well">
               <dl class="dl-horizontal">
                   <dt>Created at: </dt>
                   <dd> {{ date('M j, Y, H:i', strtotime($profile->created_at)) }}</dd>
               </dl>
                <dl class="dl-horizontal">
                    <dt>Last updated: </dt>
                    <dd> {{ date('M j, Y, H:i', strtotime($profile->updated_at)) }}</dd>
                </dl>
                <hr>
                <div class="row">
                    <div class="col-sm-6">
                        {!! Html::linkRoute('users.edit', 'Edit', array($profile->id), array('class'=>"btn btn-primary btn-block")) !!}

                    </div>
                    <div class="col-sm-6">
                        {!! Html::linkRoute('users.destroy', 'Delete', array($profile->id), array('class'=>"btn btn-danger btn-block")) !!}

                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>

    @if (Auth::check() && Auth::user()->isAdmin())
    @endsection