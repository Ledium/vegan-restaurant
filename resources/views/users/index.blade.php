@extends('main')


@section('content')
    @if (Auth::check() && Auth::user()->isAdmin())

    <div class="row">
        <div class="col-md-10">
            <h1>All profiles</h1>
        </div>


        <hr>
    </div>

    <div class="class=row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <th>#</th>
                    <th>name</th>
                    <th>surname</th>
                    <th>Adsress</th>
                    <th>date of birth</th>
                    <th>City</th>
                    <th>Country</th>
                    <th>Zip code</th>
                    <td>Email address</td>
                    <td></td>
                </thead>
                <tbody>
                @foreach($profiles as $profile)

                    <tr>
                        <th>{{$profile->id}}</th>
                        <td>{{$profile->name}}</td>
                        <td>{{$profile->surname}}</td>
                        <td>{{$profile->address}}</td>
                        <td>{{$profile->date}}</td>
                        <td>{{$profile->city}}</td>
                        <td>{{$profile->country}}</td>
                        <td>{{$profile->zip_code}}</td>
                        <td>{{$profile->email}}</td>

                        <td>{{date( 'M j, Y', strtotime($profile->created_at)) }}</td>
                        <td><a href="{{ route('users.edit', $profile->id) }}"class="btn btn-default btn-sm">edit</a></td>
                    </tr>


                 @endforeach

                </tbody>
            </table>
        </div>
    </div>

    @endif


@endsection