@extends('main')

@section('content')
    <div class="row">
        <div class="col-sm-12 col-md-10 col-md-offset-1">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th class="text-center"> Title</th>
                    <th></th>
                    <th class="text-center">Quantity</th>

                    <th class="text-center">Price</th>
                    <th class="text-center">Sum</th>

                </tr>
                </thead>
                <tbody>
                @foreach($cart->getDishes() as $item)
                    <tr>
                        <td class="col-sm-8 col-md-6">
                            <div class="media">

                                <a class=" col-md-4 col-sm-4 thumbnail pull-left" href="#">
                                    <img class="media-object"
                                         src="{{Storage::url($item->image)}}"
                                         style="width: 100px; height: 72px;">
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading col-sm-1 col-md-1 text-center">
                                        <strong>{{$item->title}}</strong></h4>
                                </div>
                            </div>
                        </td>
                        <td class="col-sm-1 col-md-1" style="text-align: center">

                            <a class="cart_quantity" href="#">
                                {!! Form::model($item, ['route' => ['cart.add', $item->id], 'method' => 'POST']) !!}

                                {{ Form::submit('+', array('class' => 'btn btn- fa fa-shopping-cart', 'style' => 'margin-top:20px'))}}
                                {!! Form::close() !!}
                                {{ $item->quantity }}

                            {!! Form::model($item, ['route' => ['cart.reduce', $item->id], 'method' => 'POST']) !!}

                            {{ Form::submit('-', array('class' => 'btn btn- fa fa-shopping-cart', 'style' => 'margin-top:20px'))}}
                            {!! Form::close() !!}



                        </td>
                        <td class="col-sm-1 col-md-1 text-center"><strong>{{$cart->get($item->id)}}</strong></td>
                        <td class="col-sm-1 col-md-1 text-center"><strong>{{$item->getPriceWithTax()}} &euro;</strong>
                        </td>
                        <td class="col-sm-1 col-md-1 text-center">
                            <strong>{{$item->getPriceWithTax() * $cart->get($item->id)}} &euro;</strong>
                        </td>
                        <td class="col-sm-1 col-md-1">
                            <a href="{{route('cart.remove',$item->id)}}">
                                <button type="button" class="btn btn-danger">
                                    <span class="fa fa-remove"></span> Remove
                                </button>
                            </a>
                        </td>
                    </tr>
                @endforeach
                <tr>


                    <td colspan="4" class="text-right"><h4>Total</h4></td>
                    <td class="text-right" colspan="2"><h4>{{$cart->getTotal()}} &euro;</h4></td>
                </tr>
                <tr>
                    <td colspan="4" class="text-right"><h4>Tax</h4></td>
                    <td class="text-right" colspan="2"><h4>{{$cart->getTotalWithTax() - $cart->getTotal()}} &euro;</h4></td>
                </tr>
                <tr>
                    <td colspan="4" class="text-right"><h3>Total with tax</h3></td>

                    <td class="text-right" colspan="2"><h3><strong>{{$cart->getTotalWithTax()}} &euro;</strong></h3></td>
                </tr>

                <tr>
                    <td colspan="6" class="text-right">
                        <a href="{{url('/')}}" class="btn btn-default">
                            <span class="fa fa-shopping-cart"></span> Continue Shopping
                        </a>
                        <a href="#"></a>


                        <form action="{{ route('orders.store') }}" method="POST">
                            {{ csrf_field() }}
                            <button class="btn btn-success"
                            type="submit">
                                <span>Checkout</span>
                            </button>
                        </form>


                    </td>
                    <td>

                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>







@endsection