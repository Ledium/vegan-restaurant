<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Notifiable;

class Dish extends Model
{
    //


    protected $fillable = [
        'title', 'description','price','image',
    ];

    public function getPriceWithTax()
    {
        return number_format($this->price * 1.21, 2);
    }

//    public function order()
//    {
//        return $this->belongsToMany(Order::class);
//    }




}
