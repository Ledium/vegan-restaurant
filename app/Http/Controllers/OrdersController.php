<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Dish;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $orders = Order::all();


        return view('orders.index')->withOrders($orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = new Order();


        $cart =  Cart::instance();

        $dishes = $cart->getDishes();


        $order->user_id = Auth::user()->id;

        $order->total =  $cart->getTotal();


        $order->save();

        foreach($dishes as $dish)
        {
            $order->dishOrder()->create([ 'dish_id' => $dish->id, 'price'=> $dish->price, 'quantity' => $cart->get($dish->id)]);
        }



        $cart->destroy();

         return redirect()->route('dishes.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $orders = Order::find($id);

        $dishes = $orders->dishOrder;
        return view('orders.show')->withdishes($dishes);


    }

    public function order()
    {

        $order = Auth::user()->orders;
        // return the view and pass in the  var we previously created
        return view('orders.order')->withorder($order);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
