<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Reservation;
use Session;


class ReservationController extends Controller
{

    protected $redirectTo = '/contact';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //


        $reservations = Reservation::latest('date')->get();


        return view('reservations.index')->withreservations($reservations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        return view('reservations.create');


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //
        $this->validate($request, array(
            'name' => 'required|max:255',
            'date' => 'required',
            'time' => 'required',
            'phone_number' => 'required',
            'number_of_people' => 'required|min:1'
        ));
        // store in the database
        $reservation = new Reservation;
        $reservation->name = $request->name;
        $reservation->date = $request->date;
        $reservation->time = $request->time;
        $reservation->phone_number = $request->phone_number;
        $reservation->number_of_people = $request->number_of_people;

//        $reservationData = $request->all();

        $reservation->save();


        Session::flash('success', 'your reservation was successfully created!');
        // redirect to another page
        return redirect()->route('reservations.create',$reservation->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $reservation = reservation::find($id);
        return view('reservations.show')->withreservation($reservation);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //
        //find the reservation and save it as a variable
        $reservation = reservation::find($id);
        // return the view and pass in the  var we previously created
        return view('reservations.edit')->withreservation($reservation);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, array(
            'name' => 'required|max:255',
            'number_of_people' => 'required',
            'phone_number' => 'required',
            'date' => 'required',
            'time' => 'required',

        ));
        //save data to database
        $reservation = reservation::find($id);



        $reservation->name = $request->input('name');
        $reservation->number_of_people = $request->input('number_of_people');
        $reservation->phone_number = $request->input('phone_number');
        $reservation->date = $request->input('date');
        $reservation->time = $request->input('time');

        $reservation->save();

        //set flash data with success message

        Session::flash('success', 'reservation has been updated successfully');


        return redirect()->route('reservations.show', $reservation->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
