<?php

namespace App\Http\Controllers;
use App\Dish;
use Illuminate\Http\Request;
use Session;


class DishesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $dishes = Dish::all();


        return view('dishes.index')->withDishes($dishes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('dishes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // validate the data
        $this->validate($request, array(
            'title' => 'required|max:255',
            'description' => 'required',
            'image' => 'required',
            'price' => 'required',
        ));
        // store in the database
        $dish = new Dish;
        if ($request->file('image'))
        {
            $path = $request->file('image')->store('public');
            $dish->image = $path;
        }
        $dish->title = $request->title;
        $dish->description = $request->description;
        $dish->price = $request->price;




        $dish->save();

        Session::flash('success', 'the dish was successfully saved!');
        // redirect to another page
        return redirect()->route('dishes.show', $dish->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $dish = Dish::find($id);
        return view('dishes.show')->withDish($dish);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //find the dish and save it as a variable
        $dish = Dish::find($id);
        // return the view and pass in the  var we previously created
        return view('dishes.edit')->withDish($dish);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, array(
            'title' => 'required|max:255',
            'description' => 'required',
            'price' => 'required',

        ));
        //save data to database
        $dish = Dish::find($id);
        if ($request->file('image'))
    {
        $path = $request->file('image')->store('public');
        $dish->image = $path;
    }






        $dish->title = $request->input('title');
        $dish->description = $request->input('description');
        $dish->price = $request->input('price');

        $dish->save();

        //set flash data with success message

        Session::flash('success', 'dish has been updated successfully');


        return redirect()->route('dishes.show', $dish->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Dish::destroy($id);
        return redirect()->route('dishes.index');
    }

    public function display()
    {
        $dishes = Dish::all();


        return view('dishes.view')->withDishes($dishes);
    }


}
