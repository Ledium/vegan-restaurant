<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpKernel\Profiler\Profile;
use App\User;

class ProfilesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //


        $users = User::all();


        return view('profile.index')->withUsers($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = user::find($id);
        return view('profile.show')->withUser($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('profile.edit')->withUser($user);
    }

    public function profile()
    {
        return $this->edit(Auth::user()->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, array(
            'name' => 'bail|max:25',
            'surname' => 'bail|max:25',
            'date' => 'date|date_format:Y-m-d|before:2000-1-1',
            'telephone' => ' digits:9|required',Rule::unique('users')->ignore(Auth::user()),
            'password' => 'min:6',
            'email' => 'required|email|max:255',Rule::unique('users')->ignore(Auth::user()->id),

        ));


        $inputData = $request->all();

        if ( !empty($inputData['password']))
        {
            $inputData['password'] = bcrypt($inputData['password']);
        } else
        {
            unset($inputData['password']);
        }


        User::find($id)->update($inputData);


        //set flash data with success message

        Session::flash('success', 'user has been updated successfully');


       return redirect()->route('user.profile');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
