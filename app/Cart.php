<?php

namespace App;


use Illuminate\Support\Collection;

class Cart extends Collection
{

    public $totalQuantity = 0;


    public static function instance()
    {
        $cart = session('cart', new static);
        return $cart;

    }
      public function getTotalQuantity()
    {
        $dishes = $this->getDishes();
        

        foreach($dishes as $dish)
        {
            $this->totalQuantity += $dish->quantity;
        }

        return $this;
    }
   

    public function addItem($itemId, $quantity)
    {
        $item = $this->get($itemId, 0);
        $this->put($itemId, $item + $quantity);
        $this->totalQuantity++;
        return $this;
    }

    public function reduceItem($itemId, $quantity)
    {
        $item = $this->get($itemId, 0);
        $this->put($itemId, $item - $quantity);
        $this->totalQuantity--;
        return $this;

    }


    public function save()
    {
        session()->set('cart', $this);
    }


    public function getTotal()
    {
        $dishes = $this->getDishes();
        $total = 0;

        foreach( $dishes as $dish)
        {
            $total += $dish->price * $this->get($dish->id);
        }

        return number_format($total, 2);
    }

   


    public function getTotalWithTax()
    {

        $dishes = $this->getDishes();
        $total = 0;

        foreach( $dishes as $dish)
        {
            $total += $dish->getPriceWithTax() * $this->get($dish->id);
        }

        return number_format($total, 2);


    }

    public function getDishes()
    {

        $dishes = Dish::whereIn('id', $this->keys())->get();

        return $dishes;

    }



    public function destroy()
    {


        $this->items = [];


        $this->save();


    }


}