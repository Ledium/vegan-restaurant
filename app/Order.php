<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id', 'total',
    ];

    public function user ()
    {
        return $this->belongsTo(User::class);

    }

    public function dishes ()
    {
        return $this->hasManyThrough(Dish::class, DishOrder::class);
    }


    public function dishOrder ()
    {
        return $this->hasMany(DishOrder::class);
    }




}
