<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Orders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('orders', function(Blueprint $table){

            $table->increments('id');
            $table->integer('user_id');
            $table->decimal('total',10,2);
            $table->timestamps();

        });

        Schema::create('dish_order', function(Blueprint $table){

            $table->increments('id');
            $table->integer('order_id');
            $table->integer('dish_id');
            $table->integer('quantity');
            $table->decimal('price',10,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('dish_order');

        Schema::drop('orders');


    }
}
