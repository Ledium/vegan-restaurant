<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('pages.welcome');
});
Route::get('protected', ['middleware' => ['admin'], function() {
    return "this page requires that you be logged in and an Admin";
}]);


Auth::routes();

Route::get('/contacts', function () {
    return view('pages.contact');
})->name('contacts');

Route::get('/home', 'HomeController@index');

Route::resource('/dishes', 'DishesController');



Route::resource('reservations', 'ReservationController');

Route::get('/orders/store', 'OrdersController@store')->name('orders.store');

Route::get('/orders/order', 'OrdersController@order')->name('orders.order');

Route::resource('orders', 'OrdersController');


Route::group(['middleware' => 'auth'], function()
{
    Route::get('/profile', 'ProfilesController@profile')->name('user.profile');
    Route::patch('/profile/{id}',  'ProfilesController@update')->name('user.update');
    Route::post('/cart/add/{dishId}', 'CartController@addItem')->name('cart.add');
    Route::post('/cart/reduce/{dishId}', 'CartController@reduceItem')->name('cart.reduce');
    Route::get('/cart/remove/{dishId}', 'CartController@removeItem')->name('cart.remove');
    Route::get('/cart', 'CartController@showCart')->name('cart');

    Route::get('/dishes/destroy/{id}','DishesController@destroy')->name('dishes.destroy');





});

Route::group(['middleware' => 'auth', 'isAdmin'], function()
{
    Route::resource('/users', 'UserProfilesController');




});

